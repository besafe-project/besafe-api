# BESAFE-API

BeSafe is a mobile application that will allow its users to report acts of violence that have been committed against them by creating alerts, which will immediately share the victim's location with their loved ones as well as nearby users. This network of users, as well as all the information collected, will allow you to define the safest routes beforehand.

## Getting started  🏎

To make it easy for you to get started with this project, here's a list of recommended next steps.

- [INSTALL WSL DEBIAN](https://www.linuxfordevices.com/tutorials/linux/install-debian-on-windows-wsl)
- [INSTALL DOCKER](https://docs.docker.com/engine/install/debian/#install-from-a-package)
- [INSTALL MAKE](https://installati.one/debian/11/make/)
- [INSTALL PHPSTORM](https://www.jetbrains.com/shop/eform/students)

## Clone project  🧬

    Create this tree project and clone the project : 

```
    - cd
    - mkdir besafe-project/besafe-api-workspace/ && cd besafe-project/besafe-api-workspace 
    - git clone https://gitlab.com/besafe-project/besafe-api.git .
```

## Define git global information    👀
    
    Change these lines, adding your personal informations (git information): 

```    
    git config --global user.email "gitlab-email" \
         &&  git config --global user.name "gitlab-name"

```

## Configure DataBase   🛢️👀

    Create a local file for the .env and change the databse url

```
    cp .env .env.local
    
    inside the file replace the database url with this : 
    
        DATABASE_URL="postgresql://root:ChangeMe@database:5432/app?serverVersion=15&charset=utf8"
    
```
## Start project    🏁
    Always run these commands to start a project : 

```
    sudo service docker start
    make install
    make start
    
```
