<?php

namespace App\Domain\Port\InfoGouv;

use App\Domain\Entity\InfoGouv;

interface RecupererInfosGouv
{
    /**
     * @return InfoGouv[]
     */
    public function recupererInfosGouvParCommune(string $codeCommune): array;

    /**
     * @return InfoGouv[]
     */
    public function recupererInfosGouvParAnnee(int $annee): array;
}
