<?php

namespace App\Domain\Port\Alerte;

use App\Domain\Entity\Alerte;

interface RecupererAlerte
{
    /**
     * @return Alerte[]
     */
    public function recupererAlertesParDepartement(string $codeDepartement): array;
}
