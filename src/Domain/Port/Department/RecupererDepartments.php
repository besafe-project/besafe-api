<?php

namespace App\Domain\Port\Department;

use App\Domain\Entity\Department;

interface RecupererDepartments
{
    /** @return Department[] */
    public function retrieveDepartments(): array;

    public function recupererDepartementParCode(string $code): ?Department;
}
