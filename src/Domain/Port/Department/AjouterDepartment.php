<?php

namespace App\Domain\Port\Department;

use App\Domain\Entity\Department;

interface AjouterDepartment
{
    public function add(Department $department): void;
}
