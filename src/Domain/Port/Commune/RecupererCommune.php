<?php

namespace App\Domain\Port\Commune;

use App\Domain\Entity\Commune;

interface RecupererCommune
{
    public function recupererCommuneParCode(string $code): ?Commune;
}
