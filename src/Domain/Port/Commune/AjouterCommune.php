<?php

namespace App\Domain\Port\Commune;

use App\Domain\Entity\Commune;

interface AjouterCommune
{
    public function addCommune(Commune $commune): void;

    public function sendData(): void;
}
