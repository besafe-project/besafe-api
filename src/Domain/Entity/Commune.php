<?php

namespace App\Domain\Entity;

class Commune
{
    private string $codeInsee;

    private string $libelle;

    private float $latitude;

    private float $longitude;

    private Department $department;

    public function __construct(
        string $codeInsee,
        string $libelle,
        float $latitude,
        float $longitude,
        Department $department
    ) {
        $this->codeInsee = $codeInsee;
        $this->libelle = $libelle;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->department = $department;
    }

    public function getCodeInsee(): string
    {
        return $this->codeInsee;
    }

    public function setCodeInsee(string $codeInsee): void
    {
        $this->codeInsee = $codeInsee;
    }

    public function getLibelle(): string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): void
    {
        $this->libelle = $libelle;
    }

    public function getLatitude(): float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude): void
    {
        $this->latitude = $latitude;
    }

    public function getLongitude(): float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): void
    {
        $this->longitude = $longitude;
    }

    public function getDepartment(): Department
    {
        return $this->department;
    }

    public function setDepartment(Department $department): void
    {
        $this->department = $department;
    }
}
