<?php

namespace App\Domain\Entity;

class InfoGouv
{
    private string $code;

    private Commune $commune;

    private int $annee;

    private string $type;

    private bool $publiee;

    private int $nbFaits;

    private float $tauxPourMille;

    private float $nbFaitsMoyDepartement;

    private float $tauxPourMilleMoyDepartement;

    public function __construct(
        string $code,
        Commune $commune,
        int $annee,
        string $type,
        bool $publiee,
        int $nbFaits,
        float $tauxPourMille,
        float $nbFaitsMoyDepartement,
        float $tauxPourMilleMoyDepartement
    ) {
        $this->code = $code;
        $this->commune = $commune;
        $this->annee = $annee;
        $this->type = $type;
        $this->publiee = $publiee;
        $this->nbFaits = $nbFaits;
        $this->tauxPourMille = $tauxPourMille;
        $this->nbFaitsMoyDepartement = $nbFaitsMoyDepartement;
        $this->tauxPourMilleMoyDepartement = $tauxPourMilleMoyDepartement;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    public function getCommune(): Commune
    {
        return $this->commune;
    }

    public function setCommune(Commune $commune): void
    {
        $this->commune = $commune;
    }

    public function getAnnee(): int
    {
        return $this->annee;
    }

    public function setAnnee(int $annee): void
    {
        $this->annee = $annee;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function isPubliee(): bool
    {
        return $this->publiee;
    }

    public function setPubliee(bool $publiee): void
    {
        $this->publiee = $publiee;
    }

    public function getNbFaits(): int
    {
        return $this->nbFaits;
    }

    public function setNbFaits(int $nbFaits): void
    {
        $this->nbFaits = $nbFaits;
    }

    public function getTauxPourMille(): float
    {
        return $this->tauxPourMille;
    }

    public function setTauxPourMille(float $tauxPourMille): void
    {
        $this->tauxPourMille = $tauxPourMille;
    }

    public function getNbFaitsMoyDepartement(): float
    {
        return $this->nbFaitsMoyDepartement;
    }

    public function setNbFaitsMoyDepartement(float $nbFaitsMoyDepartement): void
    {
        $this->nbFaitsMoyDepartement = $nbFaitsMoyDepartement;
    }

    public function getTauxPourMilleMoyDepartement(): float
    {
        return $this->tauxPourMilleMoyDepartement;
    }

    public function setTauxPourMilleMoyDepartement(float $tauxPourMilleMoyDepartement): void
    {
        $this->tauxPourMilleMoyDepartement = $tauxPourMilleMoyDepartement;
    }
}
