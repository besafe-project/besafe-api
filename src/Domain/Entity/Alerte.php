<?php

namespace App\Domain\Entity;

class Alerte
{
    private string $code;
    private string $titre;

    private string $type;
    private string $adresse;

    private Commune $commune;

    public function __construct(
        string $code,
        string $titre,
        string $type,
        string $adresse,
        Commune $commune
    ) {
        $this->code = $code;
        $this->titre = $titre;
        $this->type = $type;
        $this->adresse = $adresse;
        $this->commune = $commune;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    public function getTitre(): string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): void
    {
        $this->titre = $titre;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getAdresse(): string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): void
    {
        $this->adresse = $adresse;
    }

    public function getCommune(): Commune
    {
        return $this->commune;
    }

    public function setCommune(Commune $commune): void
    {
        $this->commune = $commune;
    }
}
