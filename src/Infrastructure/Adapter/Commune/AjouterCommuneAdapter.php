<?php

namespace App\Infrastructure\Adapter\Commune;

use App\Domain\Entity\Commune;
use App\Domain\Entity\Department;
use App\Domain\Port\Commune\AjouterCommune;
use App\Infrastructure\Doctrine\Entity\CommuneDB;
use App\Infrastructure\Doctrine\Entity\DepartmentDB;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Exception\ORMException;

class AjouterCommuneAdapter implements AjouterCommune
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @throws ORMException
     */
    public function addCommune(Commune $commune, ?bool $multipleRows = false): void
    {
        $communeDB = new CommuneDB(
            $commune->getCodeInsee(),
            $commune->getLibelle(),
            $commune->getLatitude(),
            $commune->getLongitude(),
            $this->getReferenceDepartementDB($commune->getDepartment())
        );

        $this->entityManager->persist($communeDB);
        if (!$multipleRows) {
            $this->sendData();
        }
    }

    public function sendData(): void
    {
        $this->entityManager->flush();
        $this->entityManager->clear();
    }

    /**
     * @throws ORMException
     */
    private function getReferenceDepartementDB(Department $departement): DepartmentDB
    {
        /** @var DepartmentDB $departement */
        $departement = $this->entityManager->getReference(DepartmentDB::class, $departement->getCode());
        return $departement;
    }
}
