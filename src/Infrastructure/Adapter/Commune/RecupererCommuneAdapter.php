<?php

namespace App\Infrastructure\Adapter\Commune;

use App\Domain\Entity\Commune;
use App\Domain\Port\Commune\RecupererCommune;
use App\Infrastructure\Doctrine\Entity\CommuneDB;
use Doctrine\ORM\EntityManagerInterface;

class RecupererCommuneAdapter implements RecupererCommune
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    public function recupererCommuneParCode(string $code): ?Commune
    {
        /** @var CommuneDB $communeDB */
        $communeDB = $this->entityManager->getRepository(CommuneDB::class)->find($code);

        return $communeDB?->toCommune();
    }
}
