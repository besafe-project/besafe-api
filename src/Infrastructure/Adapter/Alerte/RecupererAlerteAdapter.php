<?php

namespace App\Infrastructure\Adapter\Alerte;

use App\Domain\Entity\Alerte;
use App\Domain\Port\Alerte\RecupererAlerte;
use App\Infrastructure\Doctrine\Entity\AlerteDB;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class RecupererAlerteAdapter implements RecupererAlerte
{
    private EntityRepository $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(AlerteDB::class);
    }

    /**
     * @return Alerte[]
     */
    public function recupererAlertesParDepartement(string $codeDepartement): array
    {
        /** @var AlerteDB[] $alertesDB */
        $alertesDB = $this->repository->createQueryBuilder('alerte')
            ->innerJoin("alerte.communeDB", 'com')
            ->innerJoin("com.departmentDB", 'dep')
            ->andWhere("dep.code = :codeDepartement")
            ->setParameter('codeDepartement', $codeDepartement)
            ->getQuery()
            ->execute();

        $alertes = [];

        foreach ($alertesDB as $alerteDB) {
            $alertes[] = $alerteDB->toAlerte();
        }
        return $alertes;
    }
}
