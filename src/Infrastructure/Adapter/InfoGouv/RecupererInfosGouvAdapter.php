<?php

namespace App\Infrastructure\Adapter\InfoGouv;

use App\Domain\Entity\InfoGouv;
use App\Domain\Port\InfoGouv\RecupererInfosGouv;
use App\Infrastructure\Doctrine\Entity\InfoGouvDB;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class RecupererInfosGouvAdapter implements RecupererInfosGouv
{
    private EntityRepository $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(InfoGouvDB::class);
    }

    /**
     * @return InfoGouv[]
     */
    public function recupererInfosGouvParAnnee(int $annee): array
    {
        /** @var InfoGouvDB[] $infosGouvDB */
        $infosGouvDB = $this->repository->findBy(
            ["annee" => $annee]
        );

        $infosGouv = [];

        foreach ($infosGouvDB as $infoGouvDB) {
            $infosGouv[] = $infoGouvDB->toInfoGouv();
        }

        return $infosGouv;
    }

    public function recupererInfosGouvParCommune(string $codeCommune): array
    {
        /** @var InfoGouvDB[] $infosGouvDB */
        $infosGouvDB = $this->repository->findBy(
            ["communeDB" => $codeCommune]
        );

        $infosGouv = [];

        foreach ($infosGouvDB as $infoGouvDB) {
            $infosGouv[] = $infoGouvDB->toInfoGouv();
        }

        return $infosGouv;
    }
}
