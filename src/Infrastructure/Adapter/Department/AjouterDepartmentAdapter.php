<?php

namespace App\Infrastructure\Adapter\Department;

use App\Domain\Entity\Department;
use App\Domain\Port\Department\AjouterDepartment;
use App\Infrastructure\Doctrine\Entity\DepartmentDB;
use Doctrine\ORM\EntityManagerInterface;

class AjouterDepartmentAdapter implements AjouterDepartment
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function add(Department $department): void
    {
        $departmentDB = new DepartmentDB(
            $department->getCode(),
            $department->getLibelle()
        );
        $this->entityManager->persist($departmentDB);
        $this->entityManager->flush();
    }
}
