<?php

namespace App\Infrastructure\Adapter\Department;

use App\Domain\Entity\Department;
use App\Domain\Port\Department\RecupererDepartments;
use App\Infrastructure\Doctrine\Entity\DepartmentDB;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class RecupererDepartmentsAdapter implements RecupererDepartments
{
    private EntityRepository $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(DepartmentDB::class);
    }

    /** @return Department[] */
    public function retrieveDepartments(): array
    {
        /** @var DepartmentDB[] $departmentsDB */
        $departmentsDB = $this->repository->findAll();

        $departments = [];

        foreach ($departmentsDB as $departmentDB) {
            $departments[] = $departmentDB->toDepartment();
        }
        return $departments;
    }

    public function recupererDepartementParCode(string $code): ?Department
    {
        /** @var DepartmentDB $departementDB */
        $departementDB = $this->repository->find($code);

        return $departementDB?->toDepartment();
    }
}
