<?php

namespace App\Infrastructure\Mapper;

/**
 * @template TEntityDB
 * @template TEntity
 */
interface GenericMapperEntity
{
    /**
     * @param TEntityDB $entity
     * @return TEntity
     */
    public static function toEntity($entity);
}
