<?php

namespace App\Infrastructure\Mapper;

use App\Domain\Entity\Department;
use App\Infrastructure\Doctrine\Entity\DepartmentDB;

class DepartmentMapper implements GenericMapperEntity
{
    /**
     * @param DepartmentDB $entity
     * @return Department
     */
    public static function toEntity($entity): Department
    {
        return new Department(
            $entity->getCode(),
            $entity->getLibelle()
        );
    }
}
