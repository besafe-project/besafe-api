<?php

namespace App\Infrastructure\Mapper;

use App\Domain\Entity\InfoGouv;
use App\Infrastructure\Doctrine\Entity\InfoGouvDB;

class InfoGouvMapper implements GenericMapperEntity
{
    /**
     * @param InfoGouvDB $entity
     * @return InfoGouv
     */
    public static function toEntity($entity): InfoGouv
    {
        return new InfoGouv(
            $entity->getCode(),
            $entity->getCommuneDB()->toCommune(),
            $entity->getAnnee(),
            $entity->getType(),
            $entity->getPubliee(),
            $entity->getNbFaits(),
            $entity->getTauxPourMille(),
            $entity->getNbFaitsMoyDepartement(),
            $entity->getTauxPourMilleMoyDepartement()
        );
    }
}
