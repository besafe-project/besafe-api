<?php

namespace App\Infrastructure\Mapper;

use App\Domain\Entity\Commune;
use App\Infrastructure\Doctrine\Entity\CommuneDB;

class CommuneMapper implements GenericMapperEntity
{
    /**
     * @param CommuneDB $entity
     * @return Commune
     */
    public static function toEntity($entity): Commune
    {
        return new Commune(
            $entity->getCodeINSEE(),
            $entity->getLibelle(),
            $entity->getLatitude(),
            $entity->getLongitude(),
            $entity->getDepartmentDB()->toDepartment()
        );
    }
}
