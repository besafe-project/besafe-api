<?php

namespace App\Infrastructure\Mapper;

use App\Domain\Entity\Alerte;
use App\Infrastructure\Doctrine\Entity\AlerteDB;

class AlerteMapper implements GenericMapperEntity
{
    /**
     * @param AlerteDB $entity
     * @return Alerte
     */
    public static function toEntity($entity): Alerte
    {
        return new Alerte(
            $entity->getCode(),
            $entity->getTitre(),
            $entity->getType(),
            $entity->getAdresse(),
            $entity->getCommuneDB()->toCommune()
        );
    }
}
