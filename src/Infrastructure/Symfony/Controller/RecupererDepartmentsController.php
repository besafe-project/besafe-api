<?php

namespace App\Infrastructure\Symfony\Controller;

use App\Domain\Port\Department\RecupererDepartments;
use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandlerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RecupererDepartmentsController extends AbstractController
{
    private RecupererDepartments $retrieveDepartments;

    private ViewHandlerInterface $viewHandler;

    public function __construct(RecupererDepartments $retrieveDepartments, ViewHandlerInterface $viewHandler)
    {
        $this->retrieveDepartments = $retrieveDepartments;
        $this->viewHandler = $viewHandler;
    }

    #[Route('/departments', name: 'app_retrieve_all_departments', methods: ['GET'])]
    public function index(): Response
    {
        $departments = $this->retrieveDepartments->retrieveDepartments();
        $view = View::create($departments, Response::HTTP_OK);
        $view->setFormat('json');
        return $this->viewHandler->handle($view);
    }
}
