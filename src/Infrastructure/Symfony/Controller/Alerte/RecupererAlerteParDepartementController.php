<?php

namespace App\Infrastructure\Symfony\Controller\Alerte;

use App\Domain\Entity\Alerte;
use App\Domain\Port\Alerte\RecupererAlerte;
use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandlerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RecupererAlerteParDepartementController extends AbstractController
{
    private RecupererAlerte $recupererAlerte;

    private ViewHandlerInterface $viewHandler;

    public function __construct(RecupererAlerte $recupererAlerte, ViewHandlerInterface $viewHandler)
    {
        $this->recupererAlerte = $recupererAlerte;
        $this->viewHandler = $viewHandler;
    }

    #[Route('/alertes/departement/{codeDepartement}', name: 'app_recuperer_alertes_par_departement', methods: ['GET'])]
    public function index(string $codeDepartement): Response
    {
        $alertes = $this->recupererAlerte->recupererAlertesParDepartement($codeDepartement);
        $view = View::create($this->toAlertesOutput($alertes), Response::HTTP_OK);
        $view->setFormat('json');
        return $this->viewHandler->handle($view);
    }

    /**
     * @param Alerte[] $alertes
     * @return AlerteOutput[]
     */
    private function toAlertesOutput(array $alertes): array
    {
        $alertesOutput = [];

        foreach ($alertes as $alerte) {
            $alertesOutput[] = new AlerteOutput(
                $alerte->getCode(),
                $alerte->getTitre(),
                $alerte->getType(),
                $alerte->getAdresse(),
                $alerte->getCommune()->getCodeInsee(),
                $alerte->getCommune()->getLibelle(),
                $alerte->getCommune()->getDepartment()->getCode(),
            );
        }

        return $alertesOutput;
    }
}
