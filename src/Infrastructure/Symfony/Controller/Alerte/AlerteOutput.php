<?php

namespace App\Infrastructure\Symfony\Controller\Alerte;

class AlerteOutput
{
    private string $code;

    private string $titre;

    private string $type;

    private string $adresse;

    private string $codeINSEE;

    private string $libelleCommune;

    private string $codeDepartement;

    public function __construct(
        string $code,
        string $titre,
        string $type,
        string $adresse,
        string $codeINSEE,
        string $libelleCommune,
        string $codeDepartement
    ) {
        $this->code = $code;
        $this->titre = $titre;
        $this->type = $type;
        $this->adresse = $adresse;
        $this->codeINSEE = $codeINSEE;
        $this->libelleCommune = $libelleCommune;
        $this->codeDepartement = $codeDepartement;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    public function getTitre(): string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): void
    {
        $this->titre = $titre;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getAdresse(): string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): void
    {
        $this->adresse = $adresse;
    }

    public function getCodeINSEE(): string
    {
        return $this->codeINSEE;
    }

    public function setCodeINSEE(string $codeINSEE): void
    {
        $this->codeINSEE = $codeINSEE;
    }

    public function getLibelleCommune(): string
    {
        return $this->libelleCommune;
    }

    public function setLibelleCommune(string $libelleCommune): void
    {
        $this->libelleCommune = $libelleCommune;
    }

    public function getCodeDepartement(): string
    {
        return $this->codeDepartement;
    }

    public function setCodeDepartement(string $codeDepartement): void
    {
        $this->codeDepartement = $codeDepartement;
    }
}
