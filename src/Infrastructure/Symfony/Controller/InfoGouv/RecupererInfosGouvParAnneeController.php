<?php

namespace App\Infrastructure\Symfony\Controller\InfoGouv;

use App\Domain\Port\InfoGouv\RecupererInfosGouv;
use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandlerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RecupererInfosGouvParAnneeController extends AbstractController
{
    private RecupererInfosGouv $recupererInfosGouv;

    private ViewHandlerInterface $viewHandler;

    public function __construct(
        RecupererInfosGouv $recupererInfosGouv,
        ViewHandlerInterface $viewHandler
    ) {
        $this->recupererInfosGouv = $recupererInfosGouv;
        $this->viewHandler = $viewHandler;
    }

    #[Route('/infos-gouv/annee/{annee}', name: 'app_recuperer_infos-gouv_par_annee', methods: ['GET'])]
    public function index(int $annee): Response
    {
        $infosGouv = $this->recupererInfosGouv->recupererInfosGouvParAnnee($annee);
        $view = View::create($infosGouv, Response::HTTP_OK);
        $view->setFormat('json');
        return $this->viewHandler->handle($view);
    }
}
