<?php

namespace App\Infrastructure\Symfony\Command;

use App\Domain\Entity\Commune;
use App\Domain\Port\Commune\AjouterCommune;
use App\Domain\Port\Commune\RecupererCommune;
use App\Domain\Port\Department\RecupererDepartments;
use App\Infrastructure\Exceptions\AucunDepartementPourCeCodeException;
use Symfony\Component\Console\Attribute\AsCommand;

#[AsCommand(name: 'app:commune-france')]
class ImportCommuneCommand extends ImportEntiteCommand
{
    private const CODE_INSEE = "code_commune_INSEE";
    private const LIBELLE_COMMUNE = "nom_commune_complet";
    private const LATITUDE = "latitude";
    private const LONGITUDE = "longitude";
    private const CODE_DEPARTEMENT = "code_departement";

    private AjouterCommune $ajouterCommune;
    private RecupererDepartments $recupererDepartments;

    private RecupererCommune $recupererCommune;

    private int $counter = 0;

    private const BATCH_SIZE = 1000;

    public function __construct(
        AjouterCommune $ajouterCommune,
        RecupererDepartments $recupererDepartments,
        RecupererCommune $recupererCommune
    ) {
        parent::__construct();
        $this->ajouterCommune = $ajouterCommune;
        $this->recupererDepartments = $recupererDepartments;
        $this->recupererCommune = $recupererCommune;
    }

    /**
     * @throws AucunDepartementPourCeCodeException
     */
    protected function traitement(mixed $record): void
    {
        $code = $record[self::CODE_INSEE];
        $libelle = $record[self::LIBELLE_COMMUNE];
        $latitude = $record[self::LATITUDE];
        $longitude = $record[self::LONGITUDE];
        $codeDepartement = $record[self::CODE_DEPARTEMENT];

        if ($this->isEntiteConforme($code, $libelle, $latitude, $longitude, $codeDepartement)) {
            $departement = $this->recupererDepartments->recupererDepartementParCode($codeDepartement);

            if (!$departement) {
                throw new AucunDepartementPourCeCodeException(
                    "Aucun département trouvé pour le code " . $codeDepartement
                );
            }

            if (!$this->recupererCommune->recupererCommuneParCode($code)) {
                $this->counter++;
                $commune = new Commune(
                    $code,
                    $libelle,
                    $latitude,
                    $longitude,
                    $departement
                );
                if ($this->counter % self::BATCH_SIZE === 0) {
                    $this->ajouterCommune->addCommune($commune);
                } else {
                    $this->ajouterCommune->addCommune($commune, true);
                }
            }
        }
    }

    protected function endTraitement(): void
    {
        $this->ajouterCommune->sendData();
    }

    private function isEntiteConforme(
        string|null $code,
        string|null $libelle,
        string|null $latitude,
        string|null $longitude,
        string|null $codeDepartement
    ): bool {
        return $this->isCode($code) &&
            $this->isLibelle($libelle) &&
            $this->isLatitude($latitude) &&
            $this->isLongitude($longitude) &&
            $this->isDepartement($codeDepartement);
    }

    private function isCode(string|null $code)
    {
        return $code !== null && strlen(trim($code)) > 0;
    }

    private function isLibelle(string|null $libelle)
    {
        return $libelle !== null && strlen(trim($libelle)) > 0;
    }

    private function isLatitude(string|null $latitude)
    {
        return !empty($latitude) && is_numeric($latitude);
    }

    private function isLongitude(string|null $longitude)
    {
        return !empty($longitude) && is_numeric($longitude);
    }

    private function isDepartement(string|null $codeDepartement)
    {
        return $codeDepartement !== null && strlen(trim($codeDepartement)) > 0;
    }
}
