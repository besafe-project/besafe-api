<?php

namespace App\Infrastructure\Symfony\Command;

use App\Domain\Entity\Department;
use DateTime;
use League\Csv\Exception;
use League\Csv\Reader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

abstract class ImportEntiteCommand extends Command
{
    private InputInterface $input;

    private OutputInterface $output;
    protected function configure()
    {
        $this
            ->setDescription('Import data from a CSV file')
            ->addArgument('csv', InputArgument::REQUIRED, 'the path to the csv file to import');
        ;
    }

    /**
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->input = $input;
        $this->output = $output;

        $now = new DateTime();
        $output->writeln('<comment>Import entity start : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');

        $io = new SymfonyStyle($input, $output);
        $csvPath = $input->getArgument('csv');

        if (!file_exists($csvPath)) {
            $io->error(sprintf('CSV file not found at %s', $csvPath));
            return Command::FAILURE;
        }

        $csv = Reader::createFromPath($csvPath, 'r');
        $csv->setHeaderOffset(0); // Set the CSV header offset

        $recordCount = count($csv);
        $progressBar = new ProgressBar($output, $recordCount);

        foreach ($csv->getRecords() as $record) {
            $this->traitement($record);
            $progressBar->advance();
        }

        $this->endTraitement();

        $output->writeln('<comment>Import entity end : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');

        return Command::SUCCESS;
    }

    abstract protected function traitement(mixed $record): void;
    abstract protected function endTraitement(): void;
}
