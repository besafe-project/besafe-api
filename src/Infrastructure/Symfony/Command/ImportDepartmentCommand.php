<?php

namespace App\Infrastructure\Symfony\Command;

use App\Domain\Entity\Department;
use App\Domain\Port\Department\AjouterDepartment;
use Symfony\Component\Console\Attribute\AsCommand;

#[AsCommand(name: 'app:department-france')]
class ImportDepartmentCommand extends ImportEntiteCommand
{
    private AjouterDepartment $addDepartment;
    private const CODE_DEPARTEMENT = 'code_departement';
    private const LIBELLE_DEPARTEMENT = 'nom_departement';

    public function __construct(AjouterDepartment $addDepartment)
    {
        parent::__construct();
        $this->addDepartment = $addDepartment;
    }

    public function traitement(mixed $record): void
    {
        $code = $record[self::CODE_DEPARTEMENT];
        $libelle = $record[self::LIBELLE_DEPARTEMENT];

        $this->addDepartment->add(new Department(
            $code,
            $libelle
        ));
    }

    protected function endTraitement(): void
    {
        // nothing to do
    }
}
