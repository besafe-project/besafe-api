<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230520203421 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'ajout de la table commune';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE commune (
                code_insee VARCHAR(255) NOT NULL, 
                code_departement VARCHAR(255) DEFAULT NULL, 
                libelle VARCHAR(255) NOT NULL, 
                latitude DOUBLE PRECISION NOT NULL, 
                longitude DOUBLE PRECISION NOT NULL, 
                PRIMARY KEY(code_insee))');
        $this->addSql('CREATE INDEX IDX_E2E2D1EE8837B2D3 ON commune (code_departement)');
        $this->addSql('ALTER TABLE commune 
                ADD CONSTRAINT FK_E2E2D1EE8837B2D3 
                    FOREIGN KEY (code_departement) REFERENCES department (code) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE commune DROP CONSTRAINT FK_E2E2D1EE8837B2D3');
        $this->addSql('DROP TABLE commune');
    }
}
