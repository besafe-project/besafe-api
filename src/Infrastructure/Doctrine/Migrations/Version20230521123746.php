<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230521123746 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE alerte (
                code VARCHAR(255) NOT NULL, 
                code_commune VARCHAR(255) DEFAULT NULL, 
                titre TEXT NOT NULL, 
                type VARCHAR(255) NOT NULL, 
                adresse TEXT NOT NULL, 
                PRIMARY KEY(code))');
        $this->addSql('CREATE INDEX IDX_3AE753ADA459572 ON alerte (code_commune)');
        $this->addSql('ALTER TABLE alerte 
                ADD CONSTRAINT FK_3AE753ADA459572 
                    FOREIGN KEY (code_commune) REFERENCES commune (code_insee) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE alerte DROP CONSTRAINT FK_3AE753ADA459572');
        $this->addSql('DROP TABLE alerte');
    }
}
