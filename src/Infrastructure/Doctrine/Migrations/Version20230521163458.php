<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230521163458 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'ajout de la table infogouv';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE infogouv (
            code VARCHAR(255) NOT NULL, 
            code_commune VARCHAR(255) DEFAULT NULL, 
            annee INT NOT NULL, 
            type VARCHAR(500) NOT NULL, 
            publiee BOOLEAN NOT NULL, 
            nb_faits INT NOT NULL, 
            taux_pour_mille DOUBLE PRECISION NOT NULL, 
            nb_faits_moy_departement DOUBLE PRECISION NOT NULL, 
            taux_pour_mille_moy_departement DOUBLE PRECISION NOT NULL, 
            PRIMARY KEY(code))');
        $this->addSql('CREATE INDEX IDX_BA944587DA459572 ON infogouv (code_commune)');
        $this->addSql('ALTER TABLE infogouv 
            ADD CONSTRAINT FK_BA944587DA459572 
                FOREIGN KEY (code_commune) REFERENCES commune (code_insee) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE infogouv DROP CONSTRAINT FK_BA944587DA459572');
        $this->addSql('DROP TABLE infogouv');
    }
}
