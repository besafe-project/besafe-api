<?php

namespace App\Infrastructure\Doctrine\Entity;

use App\Domain\Entity\Alerte;
use App\Infrastructure\Mapper\AlerteMapper;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity()]
#[ORM\Table(name: "alerte")]
class AlerteDB
{
    #[ORM\Id]
    #[ORM\Column(type: "string", length: 255, unique: true, nullable: false)]
    private string $code;

    #[ORM\Column(type: "text", nullable: false)]
    private string $titre;

    #[ORM\Column(type: "string", length: 255, nullable: false)]
    private string $type;

    #[ORM\Column(type: "text", nullable: false)]
    private string $adresse;

    #[ORM\ManyToOne(targetEntity: CommuneDB::class)]
    #[ORM\JoinColumn(name: "code_commune", referencedColumnName: "code_insee")]
    private CommuneDB $communeDB;

    public function __construct(
        string $code,
        string $titre,
        string $type,
        string $adresse,
        CommuneDB $communeDB
    ) {
        $this->code = $code;
        $this->titre = $titre;
        $this->type = $type;
        $this->adresse = $adresse;
        $this->communeDB = $communeDB;
    }

    public function toAlerte(): Alerte
    {
        return AlerteMapper::toEntity($this);
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    public function getTitre(): string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): void
    {
        $this->titre = $titre;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getAdresse(): string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): void
    {
        $this->adresse = $adresse;
    }

    public function getCommuneDB(): CommuneDB
    {
        return $this->communeDB;
    }

    public function setCommuneDB(CommuneDB $communeDB): void
    {
        $this->communeDB = $communeDB;
    }
}
