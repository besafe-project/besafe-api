<?php

namespace App\Infrastructure\Doctrine\Entity;

use App\Domain\Entity\Department;
use App\Infrastructure\Mapper\DepartmentMapper;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity()]
#[ORM\Table(name: "department")]
class DepartmentDB
{
    #[ORM\Id]
    #[ORM\Column(type: "string", length: 255, unique: true, nullable: false)]
    private string $code;

    #[ORM\Column(type: "string", length: 255, nullable: false)]
    private string $libelle;

    public function __construct(string $code, string $libelle)
    {
        $this->code = $code;
        $this->libelle = $libelle;
    }

    public function toDepartment(): Department
    {
        return DepartmentMapper::toEntity($this);
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    public function getLibelle(): string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): void
    {
        $this->libelle = $libelle;
    }

    public function __toString()
    {
        return 'DepartmentDB: ' . $this->code;
    }
}
