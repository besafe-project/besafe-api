<?php

namespace App\Infrastructure\Doctrine\Entity;

use App\Domain\Entity\Commune;
use App\Infrastructure\Mapper\CommuneMapper;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity()]
#[ORM\Table(name: "commune")]
class CommuneDB
{
    #[ORM\Id]
    #[ORM\Column(type: "string", length: 255, unique: true, nullable: false)]
    private string $codeINSEE;

    #[ORM\Column(type: "string", length: 255, nullable: false)]
    private string $libelle;

    #[ORM\Column(type: "float", length: 255, nullable: false)]
    private float $latitude;

    #[ORM\Column(type: "float", length: 255, nullable: false)]
    private float $longitude;

    #[ORM\ManyToOne(targetEntity: DepartmentDB::class)]
    #[ORM\JoinColumn(name: "code_departement", referencedColumnName: "code")]
    private DepartmentDB $departmentDB;

    public function __construct(
        string $codeINSEE,
        string $libelle,
        float $latitude,
        float $longitude,
        DepartmentDB $departmentDB
    ) {
        $this->codeINSEE = $codeINSEE;
        $this->libelle = $libelle;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->departmentDB = $departmentDB;
    }

    public function toCommune(): Commune
    {
        return CommuneMapper::toEntity($this);
    }

    public function getCodeINSEE(): string
    {
        return $this->codeINSEE;
    }

    public function setCodeINSEE(string $codeINSEE): void
    {
        $this->codeINSEE = $codeINSEE;
    }

    public function getLibelle(): string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): void
    {
        $this->libelle = $libelle;
    }

    public function getLatitude(): float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude): void
    {
        $this->latitude = $latitude;
    }

    public function getLongitude(): float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): void
    {
        $this->longitude = $longitude;
    }

    public function getDepartmentDB(): DepartmentDB
    {
        return $this->departmentDB;
    }

    public function setDepartmentDB(DepartmentDB $departmentDB): void
    {
        $this->departmentDB = $departmentDB;
    }
}
