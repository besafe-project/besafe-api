<?php

namespace App\Infrastructure\Doctrine\Entity;

use App\Domain\Entity\InfoGouv;
use App\Infrastructure\Mapper\InfoGouvMapper;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity()]
#[ORM\Table(name: "infogouv")]
class InfoGouvDB
{
    #[ORM\Id]
    #[ORM\Column(type: "string", length: 255, unique: true, nullable: false)]
    private string $code;

    #[ORM\ManyToOne(targetEntity: CommuneDB::class)]
    #[ORM\JoinColumn(name: "code_commune", referencedColumnName: "code_insee")]
    private CommuneDB $communeDB;

    #[ORM\Column(type: "integer", nullable: false)]
    private int $annee;

    #[ORM\Column(type: "string", length: 500, nullable: false)]
    private string $type;

    #[ORM\Column(type: "boolean", nullable: false)]
    private bool $publiee;

    #[ORM\Column(type: "integer", nullable: false)]
    private int $nbFaits;

    #[ORM\Column(type: "float", nullable: false)]
    private float $tauxPourMille;

    #[ORM\Column(type: "float", nullable: false)]
    private float $nbFaitsMoyDepartement;

    #[ORM\Column(type: "float", nullable: false)]
    private float $tauxPourMilleMoyDepartement;

    public function __construct(
        string $code,
        CommuneDB $communeDB,
        int $annee,
        string $type,
        bool $publiee,
        int $nbFaits,
        float $tauxPourMille,
        float $nbFaitsMoyDepartement,
        float $tauxPourMilleMoyDepartement
    ) {
        $this->code = $code;
        $this->communeDB = $communeDB;
        $this->annee = $annee;
        $this->type = $type;
        $this->publiee = $publiee;
        $this->nbFaits = $nbFaits;
        $this->tauxPourMille = $tauxPourMille;
        $this->nbFaitsMoyDepartement = $nbFaitsMoyDepartement;
        $this->tauxPourMilleMoyDepartement = $tauxPourMilleMoyDepartement;
    }

    public function toInfoGouv(): InfoGouv
    {
        return InfoGouvMapper::toEntity($this);
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    public function getCommuneDB(): CommuneDB
    {
        return $this->communeDB;
    }

    public function setCommuneDB(CommuneDB $communeDB): void
    {
        $this->communeDB = $communeDB;
    }

    public function getAnnee(): int
    {
        return $this->annee;
    }

    public function setAnnee(int $annee): void
    {
        $this->annee = $annee;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getPubliee(): bool
    {
        return $this->publiee;
    }

    public function setPubliee(bool $publiee): void
    {
        $this->publiee = $publiee;
    }

    public function getNbFaits(): int
    {
        return $this->nbFaits;
    }

    public function setNbFaits(int $nbFaits): void
    {
        $this->nbFaits = $nbFaits;
    }

    public function getTauxPourMille(): float
    {
        return $this->tauxPourMille;
    }

    public function setTauxPourMille(float $tauxPourMille): void
    {
        $this->tauxPourMille = $tauxPourMille;
    }

    public function getNbFaitsMoyDepartement(): float
    {
        return $this->nbFaitsMoyDepartement;
    }

    public function setNbFaitsMoyDepartement(float $nbFaitsMoyDepartement): void
    {
        $this->nbFaitsMoyDepartement = $nbFaitsMoyDepartement;
    }

    public function getTauxPourMilleMoyDepartement(): float
    {
        return $this->tauxPourMilleMoyDepartement;
    }

    public function setTauxPourMilleMoyDepartement(float $tauxPourMilleMoyDepartement): void
    {
        $this->tauxPourMilleMoyDepartement = $tauxPourMilleMoyDepartement;
    }
}
