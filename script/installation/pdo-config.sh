docker-php-ext-configure \
      intl \
&&  docker-php-ext-install \
      pdo pdo_mysql pdo_pgsql opcache intl zip calendar dom mbstring gd xsl

pecl install apcu && docker-php-ext-enable apcu