<?php

namespace Infrastructure\Symfony\Command;

use App\Infrastructure\Doctrine\Entity\DepartmentDB;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class ImportDepartementCommandITest extends KernelTestCase
{
    private CommandTester $commandTester;

    protected function setUp(): void
    {
        parent::setUp();
        self::ensureKernelShutdown();
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('app:department-france');
        $this->commandTester = new CommandTester($command);
    }

    /**
     * @throws Exception
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        /** @var EntityManagerInterface $entityManager */
        $entityManager = self::getContainer()->get(EntityManagerInterface::class);

        /** @var DepartmentDB[] $departmentsDB */
        $departmentsDB = $entityManager->getRepository(DepartmentDB::class)->findAll();
        foreach ($departmentsDB as $departmentDB){
            $entityManager->remove($departmentDB);
        }
        $entityManager->flush();
    }

    /**
     * @throws Exception
     */
    public function test_doit_importer_tous_les_departements(): void
    {
        $cheminFichierTest = "tests/Infrastructure/Symfony/Command/csv/departements-test-france.csv";

        $this->commandTester->execute([
            'csv' => $cheminFichierTest,
        ]);

        // you can fetch the output like this
        $output = $this->commandTester->getDisplay();

        $this->commandTester->assertCommandIsSuccessful();

        $this->verifierInsertionDonnees();

        $this->assertStringContainsString('Import entity end', $output);
    }

    public function test_doit_retourner_failure_si_le_fichier_inexistant(): void
    {
        $cheminFichierTest = "inexistant.csv";

        $this->commandTester->execute([
            'csv' => $cheminFichierTest,
        ]);

        $this->assertEquals(1, $this->commandTester->getStatusCode());
    }

    /**
     * @throws Exception
     */
    private function verifierInsertionDonnees(): void
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = self::getContainer()->get(EntityManagerInterface::class);

        /** @var DepartmentDB[] $departements */
        $departements = $entityManager->getRepository(DepartmentDB::class)->findAll();

        $expected = [
            new DepartmentDB("01", "dep1"),
            new DepartmentDB("02", "dep2")
        ];

        $this->assertEquals($expected, $departements);
    }
}