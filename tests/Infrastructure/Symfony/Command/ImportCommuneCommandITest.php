<?php

namespace Infrastructure\Symfony\Command;

use App\Infrastructure\Doctrine\Entity\CommuneDB;
use App\Infrastructure\Doctrine\Entity\DepartmentDB;
use App\Infrastructure\Exceptions\AucunDepartementPourCeCodeException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class ImportCommuneCommandITest extends KernelTestCase
{
    private const CHEMIN_FICHIER_TEST = "tests/Infrastructure/Symfony/Command/csv/communes-test-france.csv";

    private CommandTester $commandTester;

    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        parent::setUp();
        self::ensureKernelShutdown();
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('app:commune-france');
        $this->commandTester = new CommandTester($command);

        /** @var EntityManagerInterface $entityManager */
        $entityManager = self::getContainer()->get(EntityManagerInterface::class);

        $dep1 = new DepartmentDB("01", "dep1");

        $entityManager->persist($dep1);
        $entityManager->flush();
    }

    /**
     * @throws Exception
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        /** @var EntityManagerInterface $entityManager */
        $entityManager = self::getContainer()->get(EntityManagerInterface::class);

        /** @var CommuneDB[] $communesDB */
        $communesDB = $entityManager->getRepository(CommuneDB::class)->findAll();
        foreach ($communesDB as $communeDB){
            $entityManager->remove($communeDB);
        }
        /** @var DepartmentDB[] $departmentsDB */
        $departmentsDB = $entityManager->getRepository(DepartmentDB::class)->findAll();
        foreach ($departmentsDB as $departmentDB){
            $entityManager->remove($departmentDB);
        }
        $entityManager->flush();
    }

    /**
     * @throws Exception
     */
    public function test_doit_importer_toutes_les_communes(): void
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = self::getContainer()->get(EntityManagerInterface::class);

        $dep123 = new DepartmentDB("123", "dep123");
        $entityManager->persist($dep123);
        $entityManager->flush();

        $this->commandTester->execute([
            'csv' => self::CHEMIN_FICHIER_TEST,
        ]);

        $output = $this->commandTester->getDisplay();

        $this->commandTester->assertCommandIsSuccessful();

        $this->verifierInsertionDonnees();

        $this->assertStringContainsString('Import entity end', $output);
    }

    public function test_doit_retourner_erreur_si_departement_inexistant(): void
    {
        $this->expectException(AucunDepartementPourCeCodeException::class);

        $this->expectExceptionMessage("Aucun département trouvé pour le code 123");

        $this->commandTester->execute([
            'csv' => self::CHEMIN_FICHIER_TEST,
        ]);
    }

    public function test_doit_retourner_failure_si_le_fichier_inexistant(): void
    {
        $this->commandTester->execute([
            'csv' => "inexistant.csv",
        ]);

        $this->assertEquals(1, $this->commandTester->getStatusCode());
    }

    /**
     * @throws Exception
     */
    private function verifierInsertionDonnees(): void
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = self::getContainer()->get(EntityManagerInterface::class);

        /** @var CommuneDB[] $departements */
        $communes = $entityManager->getRepository(CommuneDB::class)->findAll();

        $expected = [
            new CommuneDB(
                "01",
                "com1",
                1.0,
                1.0,
                new DepartmentDB("01", "dep1")
            ),
            new CommuneDB(
                "123",
                "com123",
                1.0,
                1.0,
                new DepartmentDB("123", "dep123")
            )
        ];

        $this->assertEquals($expected, $communes);
    }

}