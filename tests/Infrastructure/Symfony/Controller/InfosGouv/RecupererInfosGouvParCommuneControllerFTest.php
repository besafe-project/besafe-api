<?php

namespace Infrastructure\Symfony\Controller\InfosGouv;

use App\Infrastructure\Doctrine\Entity\CommuneDB;
use App\Infrastructure\Doctrine\Entity\DepartmentDB;
use App\Infrastructure\Doctrine\Entity\InfoGouvDB;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RecupererInfosGouvParCommuneControllerFTest extends WebTestCase
{
    public const ROUTE_API = '/besafe-api/api/infos-gouv/commune/01';

    private KernelBrowser $client;

    protected function setUp(): void
    {
        parent::setUp();
        self::ensureKernelShutdown();
        $this->client = self::createClient();

        /** @var EntityManagerInterface $entityManager */
        $entityManager = self::getContainer()->get(EntityManagerInterface::class);
        $dep1 = new DepartmentDB("01", "dep1");
        $com1 = new CommuneDB("01", "com1", 1.0, 1.0, $dep1);
        $com2 = new CommuneDB("02", "com2", 1.0, 1.0, $dep1);

        $infoGouv1 = new InfoGouvDB("01", $com1, 2017, "agression", true, 10, 1.455, 0, 0);
        $infoGouv2 = new InfoGouvDB("02", $com1, 2018, "agression", true, 10, 1.455, 0, 0);
        $infoGouv3 = new InfoGouvDB("03", $com2, 2018, "agression", true, 10, 1.455, 0, 0);

        $entityManager->persist($dep1);
        $entityManager->persist($com1);
        $entityManager->persist($com2);
        $entityManager->persist($infoGouv1);
        $entityManager->persist($infoGouv2);
        $entityManager->persist($infoGouv3);

        $entityManager->flush();
    }

    /**
     * @throws Exception
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        /** @var EntityManagerInterface $entityManager */
        $entityManager = self::getContainer()->get(EntityManagerInterface::class);

        /** @var InfoGouvDB[] $infosGouvDB */
        $infosGouvDB = $entityManager->getRepository(InfoGouvDB::class)->findAll();
        foreach ($infosGouvDB as $infoGouvDB){
            $entityManager->remove($infoGouvDB);
        }
        /** @var CommuneDB[] $communesDB */
        $communesDB = $entityManager->getRepository(CommuneDB::class)->findAll();
        foreach ($communesDB as $communeDB){
            $entityManager->remove($communeDB);
        }
        /** @var DepartmentDB[] $departmentsDB */
        $departmentsDB = $entityManager->getRepository(DepartmentDB::class)->findAll();
        foreach ($departmentsDB as $departmentDB){
            $entityManager->remove($departmentDB);
        }
        $entityManager->flush();
    }

    public function test_doit_retourner_toutes_les_infos_gouv_par_annee(): void
    {
        $this->client->request('GET', self::ROUTE_API);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertResponseIsSuccessful();
        $this->assertJson($this->client->getResponse()->getContent());

        $responseData = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertIsArray($responseData);
        $this->assertCount(2, $responseData);

        $expected = [
            [
                "code" => "01",
                "commune" => [
                    "codeInsee" => "01",
                    "libelle" => "com1",
                    "latitude" => 1.0,
                    "longitude" => 1.0,
                    "department" => [
                        "code" => "01",
                        "libelle" => "dep1"
                    ]
                ],
                "annee" => 2017,
                "type" => "agression",
                "publiee" => true,
                "nbFaits" => 10,
                "tauxPourMille" => 1.455,
                "nbFaitsMoyDepartement" => 0.0,
                "tauxPourMilleMoyDepartement" => 0.0
            ],
            [
                "code" => "02",
                "commune" => [
                    "codeInsee" => "01",
                    "libelle" => "com1",
                    "latitude" => 1.0,
                    "longitude" => 1.0,
                    "department" => [
                        "code" => "01",
                        "libelle" => "dep1"
                    ]
                ],
                "annee" => 2018,
                "type" => "agression",
                "publiee" => true,
                "nbFaits" => 10,
                "tauxPourMille" => 1.455,
                "nbFaitsMoyDepartement" => 0.0,
                "tauxPourMilleMoyDepartement" => 0.0
            ]
        ];

        $this->assertEquals($expected, $responseData);
    }
}