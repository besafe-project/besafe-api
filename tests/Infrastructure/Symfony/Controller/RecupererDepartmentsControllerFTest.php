<?php

namespace Infrastructure\Symfony\Controller;

use App\Infrastructure\Doctrine\Entity\DepartmentDB;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RecupererDepartmentsControllerFTest extends WebTestCase
{
    public const ROUTE_API = '/besafe-api/api/departments';

    private KernelBrowser $client;
    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        parent::setUp();
        self::ensureKernelShutdown();
        $this->client = self::createClient();

        /** @var EntityManagerInterface $entityManager */
        $entityManager = self::getContainer()->get(EntityManagerInterface::class);

        $dep1 = new DepartmentDB("1", "dep1");
        $dep2 = new DepartmentDB("2", "dep2");

        $entityManager->persist($dep1);
        $entityManager->persist($dep2);

        $entityManager->flush();

    }

    /**
     * @throws Exception
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        /** @var EntityManagerInterface $entityManager */
        $entityManager = self::getContainer()->get(EntityManagerInterface::class);
        /** @var DepartmentDB[] $departmentsDB */
        $departmentsDB = $entityManager->getRepository(DepartmentDB::class)->findAll();
        foreach ($departmentsDB as $departmentDB){
            $entityManager->remove($departmentDB);
        }
        $entityManager->flush();
    }

    public function test_doit_retourner_tous_les_departements(): void
    {
        $this->client->request('GET', self::ROUTE_API);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertResponseIsSuccessful();
        $this->assertJson($this->client->getResponse()->getContent());

        $responseData = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertIsArray($responseData);
        $this->assertCount(2, $responseData);

        $expected = [
            [
                "code" => "1",
                "libelle" => "dep1"
            ],
            [
                "code" => "2",
                "libelle" => "dep2"
            ]
        ];

        $this->assertEquals($expected, $responseData);
    }

}