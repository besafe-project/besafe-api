<?php

namespace Infrastructure\Symfony\Controller;

use App\Infrastructure\Doctrine\Entity\AlerteDB;
use App\Infrastructure\Doctrine\Entity\CommuneDB;
use App\Infrastructure\Doctrine\Entity\DepartmentDB;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RecupererAlertesParDepartementControllerFTest extends WebTestCase
{
    public const ROUTE_API = '/besafe-api/api/alertes/departement/01';

    private KernelBrowser $client;

    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        parent::setUp();
        self::ensureKernelShutdown();
        $this->client = self::createClient();

        /** @var EntityManagerInterface $entityManager */
        $entityManager = self::getContainer()->get(EntityManagerInterface::class);
        $dep1 = new DepartmentDB("01", "dep1");
        $dep2 = new DepartmentDB("02", "dep2");
        $com1 = new CommuneDB("01", "com1", 1.0, 1.0, $dep1);
        $com2 = new CommuneDB("02", "com2", 1.0, 1.0, $dep1);
        $com3 = new CommuneDB("03", "com3", 1.0, 1.0, $dep2);

        $alerte1 = new AlerteDB("01", "alerte1", "vol", "1 rue generale leclerc", $com1);
        $alerte2 = new AlerteDB("02", "alerte2", "agression", "2 rue generale leclerc", $com2);
        $alerte3 = new AlerteDB("03", "alerte3", "agression verbale", "2 rue generale leclerc", $com3);

        $entityManager->persist($dep1);
        $entityManager->persist($dep2);
        $entityManager->persist($com1);
        $entityManager->persist($com2);
        $entityManager->persist($com3);
        $entityManager->persist($alerte1);
        $entityManager->persist($alerte2);
        $entityManager->persist($alerte3);

        $entityManager->flush();
    }

    /**
     * @throws Exception
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        /** @var EntityManagerInterface $entityManager */
        $entityManager = self::getContainer()->get(EntityManagerInterface::class);

        /** @var AlerteDB[] $alertesDB */
        $alertesDB = $entityManager->getRepository(AlerteDB::class)->findAll();
        foreach ($alertesDB as $alerteDB){
            $entityManager->remove($alerteDB);
        }
        /** @var CommuneDB[] $communesDB */
        $communesDB = $entityManager->getRepository(CommuneDB::class)->findAll();
        foreach ($communesDB as $communeDB){
            $entityManager->remove($communeDB);
        }
        /** @var DepartmentDB[] $departmentsDB */
        $departmentsDB = $entityManager->getRepository(DepartmentDB::class)->findAll();
        foreach ($departmentsDB as $departmentDB){
            $entityManager->remove($departmentDB);
        }
        $entityManager->flush();
    }

    public function test_doit_retourner_toutes_les_alertes_du_01(): void
    {
        $this->client->request('GET', self::ROUTE_API);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertResponseIsSuccessful();
        $this->assertJson($this->client->getResponse()->getContent());

        $responseData = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertIsArray($responseData);
        $this->assertCount(2, $responseData);

        $expected = [
            [
                "code" => "01",
                "titre" => "alerte1",
                "type" => "vol",
                "adresse" => "1 rue generale leclerc",
                "codeINSEE" => "01",
                "libelleCommune" => "com1",
                "codeDepartement" => "01",
            ],
            [
                "code" => "02",
                "titre" => "alerte2",
                "type" => "agression",
                "adresse" => "2 rue generale leclerc",
                "codeINSEE" => "02",
                "libelleCommune" => "com2",
                "codeDepartement" => "01",
            ]
        ];

        $this->assertEquals($expected, $responseData);
    }


}