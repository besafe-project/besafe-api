install:
	docker compose -f ./docker/docker-compose.yml up -d --build
	docker exec -it besafe-api-container composer install -n
.PHONY: install

start:
	docker exec -it besafe-api-container symfony serve -d --no-tls
.PHONY: start

stop:
	docker exec -it besafe-api-container symfony server:stop
.PHONY: stop

coding-standart:
	docker exec -it besafe-api-container bash -c 'export PATH=$$PATH:./vendor/bin; phpcs -p -v --standard=PSR12 --ignore=./src/Infrastructure/Symfony/Kernel.php ./src; phpstan analyse ./src'
.PHONY: coding-standart

fix-phpcs:
	docker exec -it besafe-api-container bash -c 'export PATH=$$PATH:./vendor/bin; phpcbf ./src'
.PHONY: fix-phpcs

reset-db:
	docker exec -it besafe-api-container chmod +x bin/console
	docker exec -it besafe-api-container bash -c 'bin/console d:d:d --force;bin/console d:d:c;bin/console d:m:m -n'
	docker exec -it besafe-api-container bash -c 'php bin/console app:department-france import/departements-france.csv'
	docker exec -it besafe-api-container bash -c 'php -d memory_limit=256M bin/console app:commune-france import/communes-france.csv'
.PHONY: exec

tests:
	docker exec -it besafe-api-container chmod +x bin/console
	docker exec -it besafe-api-container bash -c 'bin/console d:d:d --force --env=test; bin/console d:d:c --env=test;bin/console d:m:m -n --env=test'
	docker exec -it besafe-api-container ./bin/phpunit --coverage-text
.PHONY: tests

exec:
	docker exec -it besafe-api-container bash
.PHONY: exec